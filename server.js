'use strict'
let express = require('express');
let bodyParser = require('body-parser');
let cors = require('cors');
let routes = require('./routes/routes');
let http = require('http');
let https = require('https');
let fs = require('fs');
let options = {
    key: fs.readFileSync('./api-key.pem'),
    cert: fs.readFileSync('./api-cert.pem'),
    passphrase: '3144248793',
}
let app = express();

let corsOption = {
    origin: true,
    credentials: true,
}


process.env[`NODE_TLS_REJECT_UNAUTHORIZED`] = 0;
app.use(cors(corsOption));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use("/public", express.static(__dirname + '/public'));
routes.assingRoutes(app);

http.createServer(app).listen(8001, function() {
    // console.log("http 8081");
});
https.createServer(options, app).listen(8002, function() {
    //console.log("https 443")
})