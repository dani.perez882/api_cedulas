var enviarmail = require('./utils/send_mail');


exports.dataUser = function(req, res) {


    if (Object.keys(req.body).length != 0) {

        var data_tarjeta = {
            primer_apellido: "",
            segundo_apellido: "",
            primer_nombre: "",
            segundo_nombre: "",
            Num_cedula: "",
            rh: "",
            fecha_nacimiento: "",
            sexo: "",
        }

        var barCode = req.body.data;

        let alphaAndDigits = barCode.replace(/[^\\p{A-Z}\\p{0-9}\\+\\_]+/g, " ");

        let splitStr = alphaAndDigits.split("\\s+");
        try {
            if (!alphaAndDigits.includes("P DSK")) {
                let avanzar = 3;
                let regexp_char = /([A-Z])\w+./g;
                let regExp_number = /([0-9])\d+/g;
                var DatosUserCaracteres = alphaAndDigits.match(regexp_char);
                var DatosUserNumber = alphaAndDigits.match(regExp_number);
                //console.log(DatosUserCaracteres);
                //data_tarjeta.Num_cedula = (DatosUserNumber[2].substring(8)).toString();
                if ((DatosUserNumber[2].substring(8, 9).toString()) == "0") {
                    data_tarjeta.Num_cedula = (DatosUserNumber[2].substring(9)).toString();
                    if ((DatosUserNumber[2].substring(9, 10).toString()) == "0") {
                        data_tarjeta.Num_cedula = (DatosUserNumber[2].substring(10)).toString();
                    }
                } else {
                    data_tarjeta.Num_cedula = (DatosUserNumber[2].substring(8)).toString();
                }

                data_tarjeta.primer_apellido = DatosUserCaracteres[0].toString();
                data_tarjeta.segundo_apellido = (DatosUserCaracteres[1]).toString();

                data_tarjeta.primer_nombre = (DatosUserCaracteres[2]).toString();
                let seg_name = (DatosUserCaracteres[avanzar]).toString();
                seg_name = seg_name.charAt(1);
                if (isNaN(seg_name)) {
                    data_tarjeta.segundo_nombre = (DatosUserCaracteres[avanzar]).toString();
                    avanzar += 1;
                } else {

                }
                let contiene = (DatosUserCaracteres[avanzar]).toString();
                if (contiene.indexOf("M") != -1) {
                    data_tarjeta.sexo = "Masculino"
                } else {
                    data_tarjeta.sexo = "Femenino"
                }
                data_tarjeta.rh = (DatosUserCaracteres[avanzar].substring(15)).toString();
                data_tarjeta.fecha_nacimiento = (DatosUserCaracteres[avanzar].substring(1, 5)).toString() + "-" + (DatosUserCaracteres[avanzar].substring(5, 7)).toString() + "-" + (DatosUserCaracteres[avanzar].substring(7, 9)).toString();
                res.send(data_tarjeta);



            } else {
                let avanzar = 4;
                let regexp_char = /([A-Z])\w+./g;
                let regExp_number = /([0-9])\d+/g;

                var DatosUserCaracteres = alphaAndDigits.match(regexp_char);
                var DatosUserNumber = alphaAndDigits.match(regExp_number);
                let seg_name = (DatosUserCaracteres[avanzar]).toString();
                seg_name = seg_name.charAt(1);
                if (isNaN(seg_name)) {
                    data_tarjeta.segundo_nombre = (DatosUserCaracteres[avanzar]).toString();
                    avanzar += 1;
                } else {
                    console.log("no tengo segundo nombre");
                }
                data_tarjeta.primer_apellido = DatosUserCaracteres[1].toString();
                //data_tarjeta.Num_cedula = (DatosUserNumber[1].substring(8)).toString();

                if ((DatosUserNumber[1].substring(8, 9).toString()) == "0") {
                    data_tarjeta.Num_cedula = (DatosUserNumber[1].substring(9)).toString();
                    if ((DatosUserNumber[1].substring(9, 10).toString()) == "0") {
                        data_tarjeta.Num_cedula = (DatosUserNumber[1].substring(10)).toString();
                    }
                } else {
                    data_tarjeta.Num_cedula = (DatosUserNumber[1].substring(8)).toString();
                }
                data_tarjeta.segundo_apellido = (DatosUserCaracteres[2]).toString();
                data_tarjeta.primer_nombre = (DatosUserCaracteres[3]).toString();
                data_tarjeta.rh = (DatosUserCaracteres[avanzar].substring(15)).toString();
                data_tarjeta.fecha_nacimiento = (DatosUserCaracteres[avanzar].substring(1, 5)).toString() + "-" + (DatosUserCaracteres[avanzar].substring(5, 7)).toString() + "-" + (DatosUserCaracteres[avanzar].substring(7, 9)).toString();
                //console.log(data_tarjeta.fecha_nacimiento)
                let contiene = (DatosUserCaracteres[avanzar]).toString();
                if (contiene.indexOf("M") != -1) {
                    data_tarjeta.sexo = "Masculino"
                } else {
                    data_tarjeta.sexo = "Femenino"
                }

                res.send(data_tarjeta);


            }

        } catch (e) {
            res.status(409);
            res.json({ "error": "nuevos datos encontrados", "info": e });
            enviarmail.mail(barCode);

        }


    } else {
        let e = "error desde variable";
        res.status(403);
        res.json({ "error": "403", "name": e });
        enviarmail.mail(e);
    }
}